---
title: OERSI bei Mastodon
author: "OERSI Team"
description: OERSI ist Mastodon beigetreten
date: 2022-05-03
tags: ["social media"]
---

OERSI ist nun bei Mastodon zu finden. Folgt uns um die neuesten Nachrichten von OERSI zu erhalten!

https://openbiblio.social/@oersi