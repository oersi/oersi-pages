---
title: Neustart der statischen Seiten
author: "OERSI Team"
description: Die neuen OERSI-Seiten
date: 2022-11-24
tags: ["init"]
---

In unserem letzten OERSI-Workshop haben wir beschlossen, dass die statischen Seiten von OERSI - wie die Dokumentation, die FAQs und der Blog - einheitlicher und zentraler gestaltet werden sollten. Dies soll dazu beitragen, die Nutzung von OERSI zu verbessern und die OERSI-Dokumentation leichter auffindbar zu machen.

Die neuen Seiten werden nun über Hugo und GitLab Pages zur Verfügung gestellt: [https://oersi.gitlab.io/oersi-pages](https://oersi.gitlab.io/oersi-pages)

Weitere Seiten, wie z.B. Tutorials, werden folgen.