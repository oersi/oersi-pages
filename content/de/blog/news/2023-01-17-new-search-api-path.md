---
title: Öffentlicher Pfad der Such-API
author: "OERSI Team"
description: Der neue Pfad der Public-Such-API ist verfügbar
date: 2023-01-17
categories: ["API"]
tags: ["search", "api"]
---

Zur Vorbereitung der offiziellen Veröffentlichung der stabilen Version 1.0 unserer Such-API ist diese ab sofort verfügbar unter der URL https://oersi.org/resources/api/search/...

Dies ist der gleiche Endpunkt zur Suche, der bisher unter https://oersi.org/resources/api-internal/search/... bereitsteht. Die alte URL funktioniert immer noch und kann zunächst noch einige Zeit parallel neben der neuen URL weiterlaufen. Langfristig soll nur noch die neue URL genutzt werden.

Siehe auch die [SIDRE API-Dokumentation<sup><i class="ps-1 fa-solid fa-up-right-from-square fa-xs"
aria-hidden="true"></i></sup>](https://oersi.gitlab.io/sidre/sidre-docs/api/](https://oersi.gitlab.io/sidre/sidre-docs/api/).
