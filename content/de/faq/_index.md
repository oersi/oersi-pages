---
title: "FAQ"
linkTitle: "FAQ"
description: "Häufig gestellte Fragen"
menu:
  main:
    weight: 30
type: paper
---

## Wie kann ich meine OER in OERSI einfügen?

Am besten wenden Sie sich mit der Anfrage an uns über das [Feedback-Formular](https://oersi.org/resources/services/contact). Schauen Sie zunächst, ob Ihre OER schon in der [Liste der zu ergänzenden Quellen](https://gitlab.com/oersi/oersi-etl/-/issues?label_name%5B%5D=add-source) aufgezählt sind und verlinken Sie in Ihrer Anfrage ggf. auf das entsprechende Ticket.

Grundsätzlich müssen wir die Metadaten Ihrer OER automatisiert abrufen können. Dazu können Sie
1. die Metadaten selbst bereitstellen
1. oder die Metadaten in einer bereits indexierten Quelle verzeichnen

Der letztlich zu wählende Ansatz hängt von den genutzten Technologien und den damit verbundenen Möglichkeiten sowie den bereitstehenden Ressourcen für die Umsetzung ab. Haben Sie ausreichend technisch versierte Ressourcen bietet sich 1.) an. Ist technisches Personal rar, Sie haben aber Menschen, die die Ressourcen manuell erfassen können, dann wäre 2.) die beste Wahl.

Mehr dazu in unserer [Dokumentation]({{< ref "docs/howto/connecting-oer-sources" >}}).

## Kann ich OERSI über eine API einbinden?

Ja, Sie können die Daten aus OERSI sowohl on-thy-fly abfragen und direkt in Ihrer Anwendung verwenden als auch einen kompletten (Teil-) Abzug der Daten herunterladen und weiterverwenden.

Details zur Nutzung der API in der  [SIDRE Dokumentation<sup><i class="ps-1 fa-solid fa-up-right-from-square fa-xs"
aria-hidden="true"></i></sup>](https://oersi.gitlab.io/sidre/sidre-docs/api/).

## Warum erhalte ich doppelte Ergebnisse über die API?

Niemals on-thy-fly Such-Anfragen an [https://oersi.org/resources/api/search/_search](https://oersi.org/resources/api/search/_search) stellen - diese liefern Ergebnisse von mehreren Indices. Bei einer Suche über die API muss immer auch der Index `oer_data` angegeben werden: [https://oersi.org/resources/api/search/oer_data/_search](https://oersi.org/resources/api/search/oer_data/_search)

## Darf ich das OERSI Logo verwenden?

Ja, das OERSI Logo darf in Präsentationen, auf Webseiten o.ä. verwendet werden um auf den OERSI zu verweisen.

## Welche Metadatenfelder unterstützt OERSI?

Das interne OERSI-Metadatenmodell orientiert sich weitestgehend am [Allgemeinen Metadatenprofil für Bildungsressourcen (AMB)](https://w3id.org/kim/amb/latest/), allerdings gibt es ein paar Abweichungen.

**Felder in OERSI, die nicht im AMB sind**:

- `sourceOrganization` (siehe auch https://github.com/dini-ag-kim/amb/issues/15)
- `creator.location`, `creator.affiliation.location`, `publisher.location` and `sourceOrganization.location` (siehe auch https://gitlab.com/oersi/oersi-setup/-/issues/165)
