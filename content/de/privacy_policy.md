---
title: Datenschutz
type: paper
notoc: true
---

### Datenschutzerklärung

Datenschutz hat einen hohen Stellenwert für die Technische Informationsbibliothek (TIB). Eine Nutzung der Dienstleistungen der TIB ist grundsätzlich ohne jede Angabe personenbezogener Daten möglich. Sofern eine betroffene Person besondere Services über die Internetseiten der TIB in Anspruch nehmen möchte, kann jedoch eine Verarbeitung personenbezogener Daten erforderlich werden. Ist die Verarbeitung personenbezogener Daten erforderlich und besteht für eine solche Verarbeitung keine gesetzliche Grundlage, holen wir eine Einwilligung der betroffenen Person ein. Die Verarbeitung personenbezogener Daten, beispielsweise des Namens, der Anschrift, der E-Mail-Adresse oder der Telefonnummer einer betroffenen Person erfolgt stets im Einklang mit der Datenschutz-Grundverordnung und den für die TIB geltenden landes- und institutionsspezifischen Datenschutzbestimmungen. Mittels dieser Datenschutzerklärung möchten wir die Öffentlichkeit über Art, Umfang und Zweck der von uns erhobenen, genutzten und verarbeiteten personenbezogenen Daten sowie die betroffenen Personen zustehenden Rechte informieren. Die in dieser Datenschutzerklärung verwendeten Begrifflichkeiten sind im Sinne der Europäischen Datenschutz-Grundverordnung (DSGVO) zu verstehen.

### Name und Anschrift des für die Verarbeitung Verantwortlichen

Verantwortlicher im Sinne des Datenschutzrechts ist die:

Technische Informationsbibliothek (TIB)  
Welfengarten 1 B  
30167 Hannover  
Deutschland

Tel.:  0511 762-8989,  
E-Mail:  information (at) tib.eu  
Website:   www.tib.eu

# Name und Anschrift der Datenschutzbeauftragten

Die Datenschutzbeauftragte des für die Verarbeitung Verantwortlichen ist:

Elke Brehm  
Tel.:  0511 762-8138,  
E-Mail:  datenschutz (at) tib.eu

**Postanschrift:**  
Technische Informationsbibliothek (TIB)  
Datenschutzbeauftragte  
Welfengarten 1 B  
30167 Hannover  
Deutschland

**Besuchsadresse:**  
TIB Conti-Campus  
Königsworther Platz 1 B  
30167 Hannover

Jede betroffene Person kann sich jederzeit bei allen Fragen und Anregungen zum Datenschutz direkt an unsere Datenschutzbeauftragte wenden.

### Registrierung für Dienstleistungen der TIB

Über die Internetseiten der TIB können Benutzerinnen und Benutzer sich für Dienstleistungen registrieren. Bei Registrierung erhebt die TIB personenbezogene Daten, um die Dienstleistungen für die Benutzerinnen und Benutzer erbringen zu können. Ohne Bereitstellung der Daten können die Dienstleistungen nicht in Anspruch genommen werden.
Welche Daten erhoben werden, ist abhängig von der Dienstleistung. Rechtsgrundlage für die Erhebung dieser Daten sind die Benutzungsordnung der TIB, die für die jeweilige Dienstleistung geltenden Sonderbedingungen bzw. die Einwilligung der Benutzerin oder des Benutzers. Mehr Informationen entnehmen Sie bitte den für die Dienstleistungen der TIB jeweils bereitgestellten Informationsblättern zum Datenschutz.

**Dienstleistungen der TIB mit Registrierung:**

* Anmeldung für die Benutzung der TIB vor Ort(Bibliotheksausweis)
* Anmeldung für die TIB Dokumentlieferung
* Anmeldung für die Nutzung des TIB AV-Portals
* Anmeldung für die Nutzung des OER-Recommenders

Die TIB verarbeitet und speichert personenbezogene Daten der betroffenen Person nur für den Zeitraum, der zur Erreichung des Speicherungszwecks erforderlich bzw. für den die Einwilligung erteilt ist und im Einklang mit der Datenschutz-Grundverordnung und den für die TIB geltenden landes- und institutionsspezifischen Datenschutzbestimmungen. Danach werden die personenbezogenen Daten routinemäßig und entsprechend den gesetzlichen Vorschriften gesperrt oder gelöscht. Sofern die Benutzerin oder der Benutzer die Dienstleistungen der TIB nicht mehr nutzen möchte und die TIB gegenüber der Benutzerin oder dem Benutzer keine Ansprüche mehr hat und kein sonstiger Rechtsgrund besteht, löschen wir die personenbezogenen Daten auf Anfrage. Wurde die Dienstleistung länger nicht genutzt, werden die personenbezogenen Daten nach Ablauf eines für die jeweilige Dienstleistung gesondert festgelegten Zeitraums gelöscht.

### Abonnement von Newslettern der TIB und Newsletter-Tracking

Über die Internetseiten der TIB können Benutzerinnen und Benutzer Newsletter abonnieren, um in regelmäßigen Abständen über Angebote und Neuigkeiten informiert zu werden. Hierfür ist die Registrierung unter Angabe von Anrede, Namen und E-Mail-Adresse erforderlich. An die von einer betroffenen Person erstmalig für den Newsletterversand eingetragene E-Mail-Adresse wird eine Bestätigungsmail im Double-Opt-In-Verfahren versendet, um zu prüfen, ob die Inhaberin oder der Inhaber der E-Mail-Adresse den Empfang des Newsletters autorisiert hat.

Bei der Anmeldung zum Newsletter speichert die TIB die IP-Adresse des von der betroffenen Person zum Zeitpunkt der Anmeldung verwendeten Computersystems sowie das Datum und die Uhrzeit der Anmeldung. Die Erhebung dieser Daten ist erforderlich, um den (möglichen) Missbrauch der E-Mail-Adresse einer betroffenen Person zu einem späteren Zeitpunkt nachvollziehen zu können.

Die im Rahmen einer Anmeldung zu einem Newsletter erhobenen personenbezogenen Daten werden ausschließlich zum Versand des Newsletters verwendet. Ferner können Abonnenten des Newsletters per E-Mail informiert werden, sofern dies für den Betrieb des Newsletter-Dienstes erforderlich ist, zum Beispiel im Falle von inhaltlichen oder technischen Änderungen am Newsletter. Die für den Newsletterversand erhobenen personenbezogenen Daten werden nicht an Dritte weitergegeben. Das Abonnement der TIB-Newsletter kann durch die betroffene Person jederzeit gekündigt werden. Die Einwilligung in die Speicherung personenbezogener Daten für den Newsletterversand kann jederzeit widerrufen werden. Zum Zwecke des Widerrufs der Einwilligung findet sich in jedem Newsletter ein entsprechender Link. Ferner besteht die Möglichkeit, sich jederzeit auch direkt auf der Internetseite des für die Verarbeitung Verantwortlichen vom Newsletterversand abzumelden oder dies dem für die Verarbeitung Verantwortlichen auf andere Weise mitzuteilen.

Die Newsletter der TIB enthalten sogenannte Zählpixel. Ein Zählpixel ist eine Miniaturgrafik, die in E-Mails eingebettet wird, die im HTML-Format versendet werden, um eine Logdatei-Aufzeichnung und eine Logdatei-Analyse zu ermöglichen. Anhand des eingebetteten Zählpixels kann die Technische Informationsbibliothek (TIB) erkennen, ob und wann eine E-Mail von einer betroffenen Person geöffnet und welche in der E-Mail befindlichen Links aufgerufen wurden.

Diese Informationen werden von der TIB gespeichert und ausgewertet, um den Newsletterversand und den Inhalt des Newsletters zu optimieren. Betroffene Personen können die über das Double-Opt-In-Verfahren abgegebene Einwilligungserklärung widerrufen. Nach einem Widerruf werden diese personenbezogenen Daten von der TIB gelöscht. Eine Abmeldung vom Erhalt des Newsletters deutet die TIB als Widerruf.

### Kontaktmöglichkeit über die Internetseiten

Die Internetseiten der TIB enthalten Angaben und Formulare, die eine schnelle elektronische Kontaktaufnahme per Kontaktformular oder per E-Mail auch für nicht bei der TIB registrierte Kundinnen und Kunden unter Angabe von Namen und E-Mail-Adresse ermöglichen. Die Angabe von Name und E-Mail-Adresse ist zur späteren Kontaktaufnahme nach Klärung des Anliegens der betroffenen Person erforderlich. Die per E-Mail oder über ein Kontaktformular von der betroffenen Person an die TIB übermittelten personenbezogenen Daten werden automatisch intern und nur zum Zweck der Bearbeitung oder der Kontaktaufnahme zur betroffenen Person gespeichert.

### Cookies

Die Internetseiten der TIB verwenden Cookies. Cookies sind Textdateien, welche über einen Internetbrowser auf einem Computersystem abgelegt und gespeichert werden, und dienen dazu, das Angebot der TIB nutzerfreundlicher, effektiver und sicherer zu machen.

Die meisten der erwendeten Cookies sind sogenannte Session-Cookies, die nach Ende des Besuchs automatisch gelöscht werden. Andere Cookies bleiben auf dem Endgerät der Nutzerin oder des Nutzers gespeichert, bis die Nutzerin oder der Nutzer diese löschen. Diese Cookies ermöglichen es der TIB, den Browser der Nutzerin oder des Nutzers beim nächsten Besuch wiederzuerkennen.

Die Nutzerinnen und Nutzer können das Setzen von Cookies durch die Internetseiten der TIB jederzeit durch dei Auswahl der entsprechenden Einstellungen des genutzten Internetbrowsers verhindern und dauerhaft widersprechen. Ferner können bereits gesetzte Cookies jederzeit über den Internetbrowser oder andere Softwareprogramme gelöscht werden. Dies ist in allen gängigen Internetbrowsern möglich. Deaktiviert die betroffene Person das Setzen von Cookies in dem genutzten Internetbrowser, sind unter Umständen nicht alle Funktionen der Internetseiten der TIB vollumfänglich nutzbar.

### Einsatz von Webanalyse-Tools

Wir erstellen eine anonymisierte Statistik und Analyse der Zugriffe auf unsere Webseiten.

Für die Erstellung der Webstatistik setzen wir die Open-Source-Software Matomo (ehemals PIWIK) in anonymisierter Form ein. D.h. es werden keine personenbezogenen Daten verarbeitet. Die Nutzung von Cookies in der Webanalyse-Software Matomo ist deaktiviert.

Bei der Speicherung der Nutzer-IP-Adresse werden die letzten beiden Oktetts nicht verarbeitet. Die Erfassung der Nutzer-ID ist deaktiviert. Für die Analyse werden neben dem Zugriff auf die Seite und der anonymisierten IP-Adresse noch folgende Daten erfasst: Datum und Zeit der Anfrage, Seitentitel der aufgerufenen Seite, URL der vorher aufgerufenen Seite (Referrer-URL), Bildschirmauflösung des Clientensystems, lokaler Zeitzone, URL von angeklickten und heruntergeladenen Dateien, URL von angeklickten externen Domains, Geolocation des Clienten (Land, Region, Stadt), Haupt-Sprache des benutzen Browsers, User Agent des benutzten Browsers

### Matomo-Opt-Out

Die mit Matomo erzeugten Informationen über die Benutzung dieser Webseite werden ausschließlich bei der TIB verarbeitet und gespeichert.

Sie haben die Möglichkeit zu verhindern, dass von Ihnen hier getätigte Aktionen analysiert und verknüpft werden. Dies wird Ihre Privatsphäre schützen, aber wird auch den Besitzer daran hindern, aus Ihren Aktionen zu lernen und die Bedienbarkeit für Sie und andere Benutzer zu verbessern.

In diesem Fall wird im Browser der betroffenen Person ein Opt-Out-Cookie hinterlegt, der verhindert, dass Matomo Nutzungsdaten speichert. Wenn die Cookies gelöscht werden, wird auch das Matomo Opt-Out-Cookie gelöscht. Der Widerspruch (Opt-Out) muss bei einem erneuten Besuch der Internetseite der TIB wiederholt werden.

### Erfassung von allgemeinen Daten und Informationen (Logfiles)

Die Internetseiten der TIB erfassen mit jedem Aufruf durch eine betroffene Person automatisch Informationen in so genannten Server-Log Files, die Ihr Browser automatisch an die TIB übermittelt. Dies sind:

* Browsertyp und Browserversion
* verwendetes Betriebssystem
* Referrer URL
* Hostname des zugreifenden Rechners
* IP-Adresse
* Internet-Service-Provider des zugreifenden Systems
* Uhrzeit der Serveranfrage

Diese Daten sind in der Regel nicht einer bestimmten Person zuordenbar. Eine Zusammenführung dieser Daten mit anderen Datenquellen wird nicht vorgenommen.

### Routinemäßige Löschung und Sperrung von personenbezogenen Daten

Die TIB verarbeitet und speichert personenbezogene Daten der betroffenen Person nur für den Zeitraum, der zur Erreichung des Speicherungszwecks erforderlich ist und im Einklang mit der Datenschutz-Grundverordnung und den für die TIB geltenden landes- und institutionsspezifischen Datenschutzbestimmungen. Danach werden die personenbezogenen Daten routinemäßig und entsprechend den gesetzlichen Vorschriften gesperrt oder gelöscht.

### Rechte der betroffenen Person

Sie haben jederzeit das Recht, Auskunft über die in dieser Bibliothek gespeicherten Daten, deren Herkunft und Empfänger und den Zweck der Datenverarbeitung zu erhalten, sowie auf Berichtigung oder Löschung oder auf Einschränkung der Verarbeitung oder -soweit die Verarbeitung auf Ihrer Einwilligung basiert- ein Widerrufsrecht, ggfls. ein Widerspruchsrecht sowie das Recht auf Datenübertragbarkeit. Beschwerden können jederzeit bei oben angegebener Aufsichtsbehörde vorgebracht werden. Hierzu sowie zu weiteren Fragen zum Thema personenbezogene Daten können Sie sich jederzeit an uns wenden.

### OpenStreetMap

Wir haben auf unserer Website Kartenausschnitte des Online-Kartentools "OpenStreetMap" eingebunden. Angeboten wird diese Funktion von OpenStreetMap Foundation (OSMF), St John’s Innovation Centre, Cowley Road, Cambridge, CB4 0WS, United Kingdom. Durch die Verwendung dieser Kartenfunktion wird Ihre IP-Adresse an OpenStreetMap weitergeleitet. Ferner kann Ihr Standort erfasst werden, wenn Sie dies in Ihren Geräteeinstellungen – z. B. auf Ihrem Handy – zugelassen haben. Der Anbieter dieser Seite hat keinen Einfluss auf diese Datenübertragung. Details entnehmen Sie der Datenschutzerklärung von OpenStreetMap unter folgendem Link:
https://wiki.osmfoundation.org/wiki/Privacy_Policy
