---
title: Barrierefreiheit
type: paper
notoc: true
---

*Stand der Erklärung: 01.03.2021*

Informationen über die Zugänglichkeit dieser Webseiten gemäß § 9b NBGG sowie über diesbezügliche Kontaktmöglichkeiten.

Diese Erklärung zur Barrierefreiheit gilt für den Webauftritt https://oersi.org und dessen Unterseiten.

**Diese Webseiten sind mit den Vorgaben der harmonisierten europäischen Norm EN 301 549 V2.1.2 (08-2018) und des WCAG 2.1 (Web Content Accessibility Guidelines) größtenteils vereinbar.**

### Nicht barrierefreie Inhalte:

1. Inhalte, die von oersi.org nicht oder nur durch unverhältnismäßigen Aufwand barrierefrei umgewandelt werden können.
1. Inhalte, die von Dritten bereit gestellt werden (Nutzercontent) gem. Art. 1 Abs.4 Lit. e) der Richtlinie.
1. In den Webauftritt eingebundene Informationen, insbesondere
    * Videos
    * Fotos, Grafiken, Bilder
    * PDF-Dokumente

**Die aufgeführten Inhalte sind aus folgenden Gründen nicht barrierefrei:**

Zu 1. & 2.:

Diese Inhalte sind von der Anwendung der Barrierefreiheit ausgenommen gemäß § 9 Absatz 2 NBGG.

Zu 3.:

* Videos haben keine Textalternativen.
* Fotos, Grafiken, Bilder haben zum Teil keine Textbeschreibungen.
* PDF-Dokumente sind nicht barrierefrei erstellt.

Der Webauftritt wird an die EU-Richtlinie 2016/2102 zur Umsetzung des barrierefreien Internets öffentlicher Stellen angepasst und optimiert. Die identifizierten Defizite und Mängel befinden sich aktuell in einem kontinuierlichen Verbesserungsprozess, der sukzessive abgearbeitet wird.

### Erstellung dieser Erklärung zur Barrierefreiheit

Diese Erklärung wurde am 01.03.2021 erstellt. Die Einschätzung basiert auf Selbstbewertung.

### Feedback und Kontaktangaben

Über folgenden Kontakt können Sie Mängel in Bezug auf die Einhaltung der Barrierefreiheitsanforderungen mitteilen:

Technische Informationsbibliothek (TIB)  
\- Vertrauensperson der TIB für schwerbehinderte Menschen \-  
Postfach 60 80, 30060 Hannover

Per Telefon: 0511 762-17806  
Per E-Mail: SBV (at) tib.eu

### Schlichtungsverfahren

Bei nicht zufriedenstellenden Antworten aus oben genannter Kontaktmöglichkeit können Sie bei der Schlichtungsstelle, eingerichtet bei der Landesbeauftragten für Menschen mit Behinderungen in Niedersachsen, einen Antrag auf Einleitung eines Schlichtungsverfahrens nach dem Niedersächsischen Behindertengleichstellungsgesetz (NBGG) stellen. 

Die Schlichtungsstelle nach § 9 d NBGG hat die Aufgabe, Streitigkeiten zwischen Menschen mit Behinderungen und öffentlichen Stellen des Landes Niedersachsen, zum Thema Barrierefreiheit in der IT, beizulegen. Das Schlichtungsverfahren ist kostenlos. Es muss kein Rechtsbeistand eingeschaltet werden. 

Direkt kontaktieren können Sie die Schlichtungsstelle unter:

Per Telefon: 0511/120-4010  
Per E-Mail: schlichtungsstelle (at) ms.niedersachsen.de
