---
title: Datensatz aktualisieren
type: paper
notoc: true
---

{{% pageinfo color="secondary" %}}
Proof of concept. Experimental
{{% /pageinfo %}}

Lade die Metadaten einer OER aus einer bereits angeschlossenen Quelle neu und aktualisiere diese im OERSI. So können neue OER oder Metadaten-Aktualisierungen schneller im OERSI veröffentlicht werden.

Aktueller Status:
* Quellen mit möglichem Einzelmaterial-Update
  * Edu-Sharing Instanzen www.twillo.de, oer.vhb.org
  * GitLab-Instanzen (gitlab.com, gitlab.gwdg.de, git.rwth-aachen.de)
  * github.com
  * Opencast Instanzen (video4.virtuos.uni-osnabrueck.de)
* Löschen, neu anlegen und bestehende Metadaten aktualisieren möglich.
  * Für die gegebene URL werden die Metadaten aus der angeschlossenen Quelle neu geladen bzw. geprüft, ob die Metadaten noch in der angeschlossenen Quelle enthalten sind.

Beispiele für Metadaten-URLs
* `https://www.twillo.de/edu-sharing/components/render/4ac92a56-9819-40e5-98bf-643787a9e2e3`
* `https://gitlab.com/oer/OS`

{{< form id="record-update-form" url="/resources/api-internal/import-scripts/python/update-record" submit_button_label="Update" ok_info_id="update_info_ok" failed_info_id="update_info_failed" >}}
<div class="form-group">
  <label for="recordId">Datensatz-URL</label>
  <input type="url" class="form-control" name="recordId" placeholder="URL des Datensatzes" required>
  <small class="form-text text-muted">Bitte die Metadaten-URL der Quelle des Datensatzes angeben (das ist die "mainEntityOfPage.id" in den OERSI-Metadaten).</small>
</div>
{{</ form >}}

<div id="update_info_ok" style="display: none">
{{% pageinfo color="success" %}}
Die Aktualisierung wurde erfolgreich verarbeitet und wird in der nächsten Minute indiziert und im OERSI verfügbar sein.
{{% /pageinfo %}}
</div>
<div id="update_info_failed" style="display: none">
{{% pageinfo color="warning" %}}
Die Aktualisierung war nicht erfolgreich!

Bitte beachten, dass das Einzelmaterial-Update bisher nur für einige Quellen verfügbar ist (s.o.).
{{% /pageinfo %}}
</div>
