---
title: Impressum
type: paper
notoc: true
---

Impressum zu dieser Website – zugleich Anbieterkennzeichnung gem. § 5 Digitale Dienste Gesetz (DDG)

### Herausgeber:

Technische Informationsbibliothek (TIB)  
Welfengarten 1 B, 30167 Hannover  
Postfach 6080, 30060 Hannover  

### Vertretungsberechtigte Person:

Prof. Dr. Sören Auer (Direktor der TIB)

Die Technische Informationsbibliothek (TIB) ist eine Stiftung öffentlichen Rechts des Landes Niedersachsen.

### Aufsichtsbehörde:

Ministerium für Wissenschaft und Kultur des Landes Niedersachsen

### Kontakt:

Telefon:   0511 762-8989  
Fax:   0511 762-4076  
E-Mail:   information (at) tib.eu

### Umsatzsteuer-Identifikationsnummer:

DE 214931803

### Redaktion:

Verantwortlich für den Inhalt nach § 18 Abs. 2 MStV: Axel Klinger; E-Mail:   axel.klinger (at) tib.eu
