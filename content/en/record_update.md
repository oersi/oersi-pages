---
title: Update record
type: paper
notoc: true
---

{{% pageinfo color="secondary" %}}
Proof of concept. Experimental
{{% /pageinfo %}}

(Re)load the metadata of an OER from an already connected source and update it in OERSI. This way, new OER or metadata updates can be published faster in OERSI.

Current status:
* Sources whose content can be updated
  * Edu-Sharing instances www.twillo.de, oer.vhb.org
  * GitLab instances (gitlab.com, gitlab.gwdg.de, git.rwth-aachen.de)
  * github.com
  * Opencast instances (video4.virtuos.uni-osnabrueck.de)
* Delete, create new and update existing metadata possible.
  * For the given URL, the metadata is reloaded from the connected source / checked if the metadata is still contained in the connected source.

Example Metadata-URLs
* `https://www.twillo.de/edu-sharing/components/render/4ac92a56-9819-40e5-98bf-643787a9e2e3`
* `https://gitlab.com/oer/OS`


{{< form id="record-update-form" url="/resources/api-internal/import-scripts/python/update-record" submit_button_label="Update" ok_info_id="update_info_ok" failed_info_id="update_info_failed" >}}
<div class="form-group">
  <label for="recordId">Record-URL</label>
  <input type="url" class="form-control" name="recordId" placeholder="URL of the record" required>
  <small class="form-text text-muted">Please provide the metadata URL of the record source (this is the "mainEntityOfPage.id" in the OERSI metadata).</small>
</div>
{{</ form >}}

<div id="update_info_ok" style="display: none">
{{% pageinfo color="success" %}}
The update has been successfully processed and will be indexed and available in OERSI in the next minute.
{{% /pageinfo %}}
</div>
<div id="update_info_failed" style="display: none">
{{% pageinfo color="warning" %}}
Update not successful!

Please note that the single record update is only available for some sources at the moment (see above).
{{% /pageinfo %}}
</div>
