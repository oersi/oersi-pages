---
title: "FAQ"
linkTitle: "FAQ"
description: "Frequently Asked Questions"
menu:
  main:
    weight: 30
type: paper
---

## How can I add my OER to OERSI?

The best way to contact us is via the [contact form](https://oersi.org/resources/services/contact). First check whether your source is already listed in the [List of sources to be added](https://gitlab.com/oersi/oersi-etl/-/issues?label_name%5B%5D=add-source) and link to the corresponding ticket in your request if necessary.

Basically, we need to be able to retrieve the metadata of your OER in an automated way. For this purpose you can
1. provide the metadata yourself
1. or list the metadata in an already indexed source

The approach you choose depends on the technologies used and the associated opportunities, as well as the resources available for implementation. If you have sufficient technically skilled resources, then 1.) is a good choice. If technical staff is scarce, but you have people who can enter the resources manually, then 2.) would be the best choice.



Read more in our [documentation]({{< ref "docs/howto/connecting-oer-sources" >}}).

## Can I integrate OERSI via an API?

Yes, you can query the data from OERSI on-thy-fly and use it directly in your application as well as download the data in bulk for further use.

Details on how to use the API in the [SIDRE documentation<sup><i class="ps-1 fa-solid fa-up-right-from-square fa-xs"
aria-hidden="true"></i></sup>](https://oersi.gitlab.io/sidre/sidre-docs/api/).

## Why do I get duplicate results via the API?

Never make on-thy-fly search requests to [https://oersi.org/resources/api/search/_search](https://oersi.org/resources/api/search/_search) - these return results from multiple indexes. When searching via the API, always include the index `oer_data`: [https://oersi.org/resources/api/search/oer_data/_search](https://oersi.org/resources/api/search/oer_data/_search)

## Am I allowed to use the OERSI logo?

Yes, the OERSI logo may be used in presentations, on websites or similar to refer to OERSI.

## What metadata fields are supported by OERSI?

The OERSI-internal metadata profile largely matches with the [General Metadata Profile for Educational Resources (Allgemeines Metadatenprofil für Bildungsressourcen,AMB)](https://w3id.org/kim/amb/latest/) but there are some slight differences.

**Fields in OERSI that are not specified in AMB**:

- `sourceOrganization` (see also this discussion (in German): https://github.com/dini-ag-kim/amb/issues/15)
- `creator.location`, `creator.affiliation.location`, `publisher.location` and `sourceOrganization.location` (see also: https://gitlab.com/oersi/oersi-setup/-/issues/165)
