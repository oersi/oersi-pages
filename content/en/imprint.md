---
title: Imprint
type: paper
notoc: true
---

Imprint for this website – also serves as provider identification according to § 5 Digitale Dienste Gesetz (DDG)

### Provider:

Technische Informationsbibliothek (TIB)  
Welfengarten 1 B, 30167 Hannover  
Postfach 6080, 30060 Hannover

### Authorized Representative:

Prof. Dr. Sören Auer (Direktor der TIB)

Technische Informationsbibliothek (TIB) is a foundation of public law of the state of Lower Saxony.

### Responsible Supervisory Authority:

Ministry for Science and Culture of Lower Saxony


### Contact:

Customer service phone:   +49 511 762-8989  
Central information desk phone:   +49 511 762-2268  
Fax:   +49 511 762-4076  
E-Mail:   information (at) tib.eu

### VAT (sales tax) registration number:

DE 214931803

### Editorial Office:

Axel Klinger; email:   axel.klinger (at) tib.eu
