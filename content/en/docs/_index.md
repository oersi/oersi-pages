
---
title: "Documentation"
linkTitle: "Documentation"
weight: 20
description: "Documentation pages for the OER Search Index (OERSI)"
---

This documentation contains some OERSI specific explanations. For the general technical documentation, please visit the [SIDRE documentation<sup><i class="ps-1 fa-solid fa-up-right-from-square fa-xs" aria-hidden="true"></i></sup>](https://oersi.gitlab.io/sidre/sidre-docs/).
