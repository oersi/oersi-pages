---
title: "Technical SIDRE Docs"
weight: 50
icon: fa-solid fa-up-right-from-square fa-xs
manualLink: https://oersi.gitlab.io/sidre/sidre-docs/
manualLinkTarget: _blank
description: "Technical documentation for the Search Index software system (SIDRE)"
---

For the general technical documentation, please visit the [SIDRE documentation<sup><i class="ps-1 fa-solid fa-up-right-from-square fa-xs" aria-hidden="true"></i></sup>](https://oersi.gitlab.io/sidre/sidre-docs/).
