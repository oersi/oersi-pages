---
categories: ["Howto"]
tags: ["connecting OER"] 
title: "Connecting OER Sources"
linkTitle: "Connecting OER Sources"
weight: 10
description: >
  How to integrate sources of OER into OERSI
---

We are interested in more sources that contain OERs in the higher education context and welcome [any suggestions](https://oersi.org/resources/services/contact).

In order to include your OER in OERSI, the metadata of the OER must be accessible for our automatic harvesting process. In principle, we are very flexible when it comes to the inclusion of sources in OERSI, i.e. there are multiple ways to connect them.

## 1. Providing the metadata of your OER

For the structured description of teaching/learning resources, we use a metadata schema that conforms to the "[General Metadata Profile for Educational Materials](https://w3id.org/kim/amb/latest/)" (German: "Allgemeines Metadatenprofil für Bildungsmaterialien" - AMB), which is based on schema.org. There it is also explained how to provide the structured data. A transformation of your existing format into our AMB-schema is usually also possible.

The OERSI fork of the AMB can be found [here](https://gitlab.com/oersi/oersi-schema) and the adaptations to the AMB are listed [here]({{< ref "faq" >}}#what-metadata-fields-are-supported-by-oersi).

### 1.1. schema.org + sitemap.xml embedded in HTML

A good general solution - also for SEO - is to provide structured metadata as embedded JSON-LD in the OER itself in combination with a [sitemap](https://www.sitemaps.org/) that lets us find the individual resources.

### 1.2. JSON-API

The provision of JSON via an HTTP-based API is also a good solution. This is much more performant and resource-efficient if several data records can be queried at once and only the raw metadata is transferred.

### 1.3. Alternatives

Since not all sources can implement the outlined approaches, we also support quite a few other approaches:

* XML via OAI-PMH
* HTML + Scraping
* Provision of a bulk file with the structured data (csv, JSON)
* ..?

## 2. Manual listing in an already indexed source

Another option is to list your OER in a source that is already indexed in OERSI. Some example repositories for this are: [ZOERR], [twillo] and [ORCA.nrw].

[ZOERR]: https://www.zoerr.de
[twillo]: https://www.twillo.de
[ORCA.nrw]: https://www.orca.nrw
