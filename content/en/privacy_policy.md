---
title: Privacy Policy
type: paper
notoc: true
---

### Privacy Statement

Data protection is of high priority for the Technische Informationsbibliothek (TIB). As a general rule, the use of TIB’s services does not require the provision of any personal data. However, the processing of personal data may be required where a data subject wants to use special services via the TIB’s web pages. Where the processing of personal data is required and where there is no legal basis for such processing, we shall obtain the data subject’s consent. The processing of personal data, such as for example the data subject’s name, address, email address, or telephone number shall always be carried out in accordance with the General Data Protection Regulation (GDPR) and the state and institution-specific data protection rules and regulations applicable to the TIB. This privacy statement serves to inform the public about the nature, scope and purpose of the personal data we collect, use and process, as well as of the rights data subjects are entitled to. The terms used in this privacy statement are to be understood within the meaning of the European General Data Protection Regulation (GDPR)

### Name and address of the controller

The controller under data protection law shall be:

Technische Informationsbibliothek (TIB)  
Welfengarten 1 B  
30167 Hannover  
Germany

Tel.:  0511 762-8989,  
E-Mail:  information (at) tib.eu  
Website:   www.tib.eu

### Name and address of the data protection officer

The data protection officer of the controller shall be:

Elke Brehm  
Tel.:  0511 762-8138,  
E-Mail:  datenschutz (at) tib.eu

**Postal address:**  
Technische Informationsbibliothek (TIB)  
data protection officer  
Welfengarten 1 B  
30167 Hannover  
Germany

**Visiting Address:**  
TIB Conti-Campus  
Königsworther Platz 1 B  
30167 Hannover

Any data subject may contact our data protection officer directly regarding any and all questions and suggestions regarding data protection at any time.

### Registration for TIB services

Users may register for services via TIB’s web pages. When registering, TIB collects personal data in order to be able to provide such services to users. The services cannot be used without providing the required data.  
Which data is collected depends on the service. The legal basis for the collection of such data shall be the Terms of use of the TIB, the Special Conditions applicable to the respective service, or the consent given by the user. For further information, please refer to the information sheets on data protection provided for the respective services offered by the TIB.

**TIB services with registration:**

* Registration for the local use of the TIB (library card)
* Registration for the TIB Document Delivery
* Registration for the use of the TIB AV Portal
* Registration for the use of the oer-recommender

The TIB processes and stores the data subject’s personal data only for the period of time necessary to achieve the purpose of the storage or for the period of time for which the consent was given and in compliance with the General Data Protection Regulation as well as the state and institution-specific data protection rules and regulations applicable to the TIB. Thereafter, the personal data shall routinely be blocked or deleted in accordance with the statutory provisions. If the user no longer wishes to use the services of the TIB and in the absence of any claims of the TIB against the user and of any other legal basis for storage, we shall delete the personal data upon request. Where the user does not use the service for a prolonged period of time, the personal data will be deleted after the expiry of a period separately specified for the respective service.

### Subscription to TIB newsletters and newsletter tracking

Users can subscribe to newsletters via the TIB web pages in order to receive information on offers and news at regular intervals. This shall require registration providing title, name and email address. Using the double opt-in procedure, a confirmation email shall be sent to the email address first registered by a data subject for the purpose of receiving the newsletter, in order to check whether the owner of the email address has authorised receipt of the newsletter.

When registering for the newsletter, TIB stores the IP address of the computer system used by the data subject at the time of registration, as well as the date and time of the registration. Collection of this data is necessary in order to be able to trace any (possible) misuse of the email address of a data subject at a later date.

The personal data collected during registration for a newsletter are used exclusively for the purpose of sending the newsletter. In addition, subscribers of the newsletter can be informed by email if this is necessary for the operation of the newsletter service, e.g. in the event of changes in content or technical aspects of the newsletter. The personal data collected for sending the newsletter will not be passed on to third parties. Subscription to the TIB newsletter can be terminated by the data subject at any time. Consent to the storage of personal data for sending the newsletter can be withdrawn at any time. For the purpose of withdrawal of consent a link is provided in each newsletter . Furthermore, it is possible to unsubscribe from the newsletter at any time directly on the website of the controller or to inform the controller thereof in any other way.

The TIB newsletters contain so-called tracking pixels. A tracking pixel is a miniature graphic embedded in emails sent in the HTML format to enable log file recording and log file analysis. Based on the embedded tracking pixel, the Technische Informationsbibliothek (TIB) is able to identify if and when an email was opened by a data subject, and which links in the email were clicked.

This information is stored and evaluated by the TIB in order to optimise the distribution and the content of the newsletter. Data subjects may withdraw the declaration of consent given via the double opt-in procedure. Following withdrawal, this personal data will be deleted by the TIB. TIB shall construe any unsubscribing of the newsletter to constitute a withdrawal.

### Contact possibility via the web pages

The TIB’s web pages contain information and forms that enable quick electronic contacting via contact form or by email providing name and email address also for customers not registered with the TIB. The indication of name and email address is necessary to establish contact following clarification of the data subject’s concerns. The personal data transmitted by the data subject to the TIB via email or via a contact form are automatically stored internally and only for the purpose of processing or contacting the data subject.

### Cookies

The TIB websites use cookies. Cookies are text files that are placed and stored on a computer system via an Internet browser and serve to render the offer of the TIB more user-friendly, effective and secure.

Most of the cookies used are so-called “session cookies”, which are automatically deleted at the end of the visit. Other cookies remain stored on the user’s terminal device until he or she deletes them. These cookies enable the TIB to recognise the user’s browser on his or her next visit.

Users can prevent and permanently object to the setting of cookies by TIB websites at any time by choosing the corresponding settings of the Internet browser used. Furthermore, cookies already set can be deleted at any time with the Internet browser or by other software programs. This is possible in all common Internet browsers. If the data subject deactivates the setting of cookies in the Internet browser used, the TIB web pages may not function properly.

### Use of web analysis tools

For the creation of the web statistics we use the open source software Matomo (formerly PIWIK) in anonymized form. This means that no personal data is processed. The use of cookies in the web analysis software Matomo is deactivated.

When saving the user IP address, the last two octets are not processed. The collection of the user ID is deactivated. For the analysis, the following data is collected in addition to the access to the site and the anonymized IP address: Date and time of the request, page title of the requested page, URL of the previously requested page (referrer URL), screen resolution of the client system, local time zone, URL of clicked and downloaded files, URL of clicked external domains, geolocation of the client (country, region, city), main language of the used browser, user agent of the used browser.

### Matomo-Opt-Out

The information generated with Matomo about the use of this website is processed and stored exclusively with the TIB.

You may choose to prevent this website from aggregating and analyzing the actions you take here. Doing so will protect your privacy, but will also prevent the owner from learning from your actions and creating a better experience for you and other users.

In this case, an opt-out cookie is placed in the browser of the data subject, which prevents Matomo from storing usage data. When the cookies are deleted, the Matomo opt-out cookie is also deleted. Such objection (opt-out) must be redeclared when visiting the TIB website again

### Collection of general data and information (logfiles)

Whenever a data subject calls the TIB web pages, these pages automatically collect information in so-called server log files, which your browser automatically transmits to the TIB. This is:

* Browser type and browser version
* Operating system used
* Referrer URL
* Hostname of the accessing computer
* IP address
* Internet service provider of the accessing system
* Time of the server request

As a general rule, this data is not attributable to a particular person. This data will not be merged with other data sources.

### Routine deletion and blocking of personal data

The TIB processes and stores the data subject’s personal data only for the period necessary to achieve the purpose of such storage and in accordance with the General Data Protection Regulation and the country and institution-specific data protection regulations applicable to the TIB. Thereafter, the personal data will routinely be blocked or deleted in accordance with the statutory provisions.

### Rights of the data subject

You shall at any time be entitled to obtain information about the data stored in this library, its origin and recipient and about the purpose of such data processing, as well as to rectification or erasure or restriction of processing or – to the extent that such processing is based on your consent – a right of withdrawal, possibly a right of objection and the right to data portability. Complaints may be lodged with the above-mentioned supervisory authority. You can contact us at any time for further questions on the subject of personal data.

### OpenStreetMap

We have integrated map sections of the online map tool "OpenStreetMap" on our website. This function is offered by the OpenStreetMap Foundation (OSMF), St John's Innovation Centre, Cowley Road, Cambridge, CB4 0WS, United Kingdom. By using this map feature, your IP address is forwarded to OpenStreetMap. Your location can also be captured if you have enabled this in your device settings, such as your mobile phone. The provider of this site has no influence on this data transfer. For details, please refer to the OpenStreetMap privacy policy at the following link:
https://wiki.osmfoundation.org/wiki/Privacy_Policy
