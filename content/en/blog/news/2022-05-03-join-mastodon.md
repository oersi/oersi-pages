---
title: OERSI at Mastodon
author: "OERSI Team"
description: OERSI has joined Mastodon
date: 2022-05-03
tags: ["social media"]
---

OERSI can now be found on Mastodon. Follow us to get the latest news from OERSI!

https://openbiblio.social/@oersi