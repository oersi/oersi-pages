---
title: Relaunch static pages
author: "OERSI Team"
description: Launching the new OERSI pages
date: 2022-11-24
tags: ["init"]
---

In our last OERSI workshop, we decided that the static pages of OERSI - such as documentation, FAQs and the blog - should be made more consistent and centralized. This should help improve the use of OERSI and make the OERSI docs easier to find.

The new pages are now being made available via Hugo and GitLab Pages: [https://oersi.gitlab.io/oersi-pages](https://oersi.gitlab.io/oersi-pages)

More pages, such as tutorials, will follow.