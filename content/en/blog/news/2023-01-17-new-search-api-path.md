---
title: Public path of the search API
author: "OERSI Team"
description: The new path of the Public Search API is available
date: 2023-01-17
categories: ["API"]
tags: ["search", "api"]
---

In preparation for the official release of the stable version 1.0 of our search API, it is now available at URL https://oersi.org/resources/api/search/...

This is the same endpoint for searching that was available at https://oersi.org/resources/api-internal/search/... up to now. The old URL still works and can continue to run in parallel with the new URL for some time. In the long term, only the new URL will be offered.

See also the [SIDRE API-Documentation<sup><i class="ps-1 fa-solid fa-up-right-from-square fa-xs"
aria-hidden="true"></i></sup>](https://oersi.gitlab.io/sidre/sidre-docs/api/](https://oersi.gitlab.io/sidre/sidre-docs/api/).
