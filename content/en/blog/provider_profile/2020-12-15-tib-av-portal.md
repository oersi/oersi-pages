---
title: "New Provider: TIB AV-Portal"
author: "OERSI Team"
description: Provider Profile of TIB AV-Portal
date: 2020-12-15
categories: ["provider_profile"]
tags: ["provider", "activated"]
draft: true
---

![TIB AV-Portal Logo](https://av.tib.eu/resources/content/sites/av.tib.eu/base/logos/logo-dark-mode.png)

| **TIB AV-Portal**   | |
| -------- | -------- |
| **URL**     | https://av.tib.eu/     |
| **Indexed since** | 2020-12-15 |
| **Host**     | [TIB – Leibniz Information Centre for Science and Technology and University Library](https://www.tib.eu/) |
| **Focus**     | cross-disciplinary scientific videos |
| **Description**     | The TIB AV-Portal is an open access platform for scientific videos with a focus on technology as well as architecture, chemistry, computer science, mathematics and physics. Over time, the scope of the portal has expanded to include a wider range of scientific disciplines, including the humanities and social sciences, economics, law and medicine (see subject page). Users of the portal have access to a variety of video content, including lecture and conference recordings, simulations, animations, experiments, interviews and video abstracts as well as open educational resources. |
| **Used API**     | Internal OAI-PMH, Datacite Format     |
| **Links**     | [Find resources in OERSI](https://oersi.org/resources?provider=%5B%22TIB+AV-Portal%22%5D) |
| | [Transformation Workflow Code](https://gitlab.com/oersi/oersi-import-scripts/-/blob/master/python/TIBAVPortalImport.py?ref_type=heads)     |