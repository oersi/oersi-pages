---
title: Accessibility Statement
type: paper
notoc: true
---

*(Date of this declaration 01 March 2021)*

Information on the accessibility of these web pages and its contact details in accordance with § 9b NBGG

This accessibility statement applies to https://oersi.org and its sub-pages.

**These websites are mostly compatible with the requirements of the harmonized European standard EN 301 549 V2.1.2 (08-2018) and WCAG 2.1 (Web Content Accessibility Guidelines).**

### Non-accessible content:

1. contents that the oersi.org is unable to convert into barrier-free content or can only do so at disproportionate expense
1. contents provided by third parties (user content) pursuant to Art. 1 (4) (e) of the Directive
1. information integrated into the website, in particular
    * Videos
    * Photos, Graphics, Images
    * PDF documents

**The listed contents are not barrier-free for the following reasons:**

To 1 & 2.:

These contents are excluded from the application of accessibility according to § 9 section 2 NBGG.

To 3:

* Videos have no text alternatives.
* Photos, graphics, images partly have no text descriptions.
* PDF documents are not created barrier-free.

The website is being adapted and optimized to comply with the EU Directive 2016/2102 on the implementation of a barrier-free Internet for public sector bodies. The identified deficits and deficiencies are currently undergoing a continuous improvement process that will be successively processed

### Establishment of this Accessibility Statement

This statement was established on 01 March 2021.

The assessment is based on self-assessment.

### Feedback and contact details

You can report cases of non-compliance with accessibility requirements to us.

Contact:

Technische Informationsbibliothek (TIB)  
\- Vertrauensperson der TIB für schwerbehinderte Menschen \-  
Postfach 60 80, 30060 Hannover

Phone: +49 (0)511 762-17806  
EMail: SBV (at) tib.eu

### Arbitration procedure

If you are not satisfied with the answers of our above-mentioned contact point, you can contact the arbitration office, which is located at the State Representative for People with Disabilities in Lower Saxony, for the initiation of an arbitration procedure in accordance with the Niedersächsisches Behindertengleichstellungsgesetz (Disability Equality Act of Lower Saxony) (NBGG).

The arbitration office has the task according to § 9 d NBGG of settling disputes between people with disabilities and public institutions of the state of Lower Saxony on the topic of accessibility in IT.

The arbitration procedure is free of charge.

No Legal Counsels are required.

You can contact the arbitration office directly at  
Phone: +49 (0)511/120-4010  
EMail: schlichtungsstelle (at) ms.niedersachsen.de
