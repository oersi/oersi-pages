# OER Search Index Pages

Static Pages of the Open Educational Resources Search Index for Higher Education (OERSI). Contains documentation, FAQs, blog etc

---

Read at https://oersi.org/resources/pages/

---

# Create and Update content

* pages can be created and updated as markdown files in the [content](content) directory - see details in [Docsy - Adding Content]
* there are separate content folders for English ([content/en](content/en)) and German ([content/de](content/de)) content
* add page metadata frontmatter - see details in [Docsy - Page frontmatter]
* the docs-subdirectory will only be maintained in English language - therefore the docs-content in the German content folder is only linked to the English content and we don't have to maintain duplicated files
* If your content is still in progress and shouldn't be published yet, use Hugo's [draft-feature](https://gohugo.io/getting-started/usage/#draft-future-and-expired-content) to prevent publication on Prod. Your draft will be deployed on [Test](https://develop.oersi.de/resources/pages/) (internal) and can be reviewed there.

# Technologies

OERSI Pages created with [Hugo] and [GitLab Pages]. Hugo Theme [docsy](https://www.docsy.dev) is used.

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml).

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install](https://www.docsy.dev/docs/get-started/other-options/#install-hugo) Hugo Extended
2. [Install Docsy dependencies](https://www.docsy.dev/docs/get-started/other-options/#prerequisites)
   1. [Install latest Node LTS release](https://www.docsy.dev/docs/get-started/other-options/#node-get-the-latest-lts-release)
   2. [Install PostCSS](https://www.docsy.dev/docs/get-started/other-options/#install-postcss)
   3. `cd themes/docsy`
   4. `npm install`
3. Preview your project: `hugo server` (or `hugo --buildDrafts server` including your draft-pages)
4. Add content
5. Generate the website: `hugo` (optional)

Read more at the [Hugo](https://gohugo.io/getting-started/) or [Docsy](https://www.docsy.dev/docs/) documentation.

### Preview your site

If you clone or download this project to your local computer and run `hugo server`,
your site can be accessed under `localhost:1313/oersi-pages`.

The theme used is adapted from https://www.docsy.dev/.

## Production Build

Change the `baseURL` in config.toml to your production-url before building.

GitLab-CI-example:
```
build-pages-prod:
  image: registry.gitlab.com/pages/hugo/hugo_extended:latest
  stage: build
  script:
    ...
    - sed -i "s#baseURL = .*#baseURL = \"https://oersi.org/resources/pages\"#" config.toml
    - hugo
```

[ci]: https://about.gitlab.com/gitlab-ci/
[hugo]: https://gohugo.io
[GitLab Pages]: https://docs.gitlab.com/ee/user/project/pages/
[Docsy - Adding Content]: https://www.docsy.dev/docs/adding-content/content/
[Docsy - Page frontmatter]: https://www.docsy.dev/docs/adding-content/content/#page-frontmatter
