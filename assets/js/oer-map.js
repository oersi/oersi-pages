class OerMap {

  regionColors = ['#006D2C', '#298651', '#52A076', '#7AB99C', '#A3D3C1', '#CCECE6', '#FFF'];

  layerData = [];
  oerCountsByRegion = [];
  boundsColors = [];
  updateInfoText = function (props) {
    return '<h4>Number of OER</h4>' +  (props ?
      '<b>' + props.names["en"] + '</b><br />' + props.oerCount + ' Resources'
      : 'Hover over a state');
  };

  constructor(countryRestriction = null, countrySearch = true, regionSearch = true) {
    this.#updateBoundsColors(10000);
    this.countryRestriction = countryRestriction;
    this.countrySearch = countrySearch;
    this.regionSearch = regionSearch;
  }

  addBaseLayer(layerName, geoJsonData, isActiveByDefault=true) {
    this.layerData.push({"name": layerName, "geoJsonData": geoJsonData, "isActiveByDefault": isActiveByDefault});
  }

  initMap() {
    const map = L.map('map');
    map.options.minZoom = 2;

    this.info = this.#initInfo();
    this.info.addTo(map);

    this.legend = this.#initLegend();
    this.legend.addTo(map);

    const that = this;
    const onEachFeature = function(feature, layer) {
      layer.on({
        mouseover: function(e) {that.highlightFeature(e)},
        mouseout: function(e) {that.resetHighlight(e)},
        click: function(e) {that.highlightFeature(e)}
      });
    };
    const regionStyle = function(feature) {
      return {
        fillColor: feature.properties.regionColor ? feature.properties.regionColor : '#FFF',
        weight: 1,
        opacity: 1,
        color: 'grey',
        fillOpacity: 0.7
      };
    };
    this.layerData.forEach(data => {
      const layer = L.geoJson(data["geoJsonData"], {style: regionStyle, onEachFeature});
      if (data["isActiveByDefault"]) {
        layer.addTo(map);
        map.fitBounds(layer.getBounds());
      }
      data["layer"] = layer;
    });
    if (this.layerData.length > 1) {
      L.control.layers(Object.assign({}, ...this.layerData.map((l) => ({[l.name]: l.layer})))).addTo(map);
    }

    this.#loadOerCountFromOersi();
  }

  #initInfo() {
    const info = L.control();
    info.onAdd = function (map) {
      this._div = L.DomUtil.create('div', 'info'); // create a div with a class "info"
      this.update();
      return this._div;
    };
    const that = this;
    info.update = function (props) {
      this._div.innerHTML = that.updateInfoText(props);
    };
    return info;
  }

  #initLegend() {
    const legend = L.control({position: 'bottomright'});
    legend.onAdd = function (map) {
      this._div = L.DomUtil.create('div', 'info legend');
      this.update();
      return this._div;
    };
    const _this = this;
    legend.update = function (props) {
      let grades = _this.boundsColors?.slice().reverse();
      let html = "";
      for (let i = 0; i < grades.length; i++) {
        html +=
          '<i style="background:' + _this.#getRegionColor(grades[i] + 1) + '"></i> ' +
          grades[i] + (grades[i + 1] ? '&ndash;' + grades[i + 1] + '<br>' : '+');
      }
      this._div.innerHTML = html
    }
    return legend;
  }

  #updateBoundsColors(upperBoundColors) {
    this.boundsColors = [1, 2, 6, 12, 32, 0].map(e => {
      if (e === 0) return 0;
      const v = upperBoundColors / e;
      const factor = v > 2000 ? 1000 : 100;
      return Math.floor(upperBoundColors / (factor * e)) * factor
    });
  }

  #getRegionColor(oerCount) {
    if (oerCount === 0) {
      return this.regionColors[this.regionColors.length - 1];
    }
    for (let i = 0; i < this.boundsColors.length; i++) {
      if (oerCount > this.boundsColors[i]) {
        return this.regionColors[i];
      }
    }
    return this.regionColors[this.regionColors.length - 1];
  }

  highlightFeature(e) {
    const layer = e.target;
    this.layerData.forEach(data => {
      data["layer"].resetStyle();
    })
    layer.setStyle({
      weight: 5,
      color: '#666',
      dashArray: '',
      fillOpacity: 0.7
    });

    layer.bringToFront();
    this.info.update(layer.feature.properties);
  }
  resetHighlight(e) {
    this.layerData.forEach(data => {
      data["layer"].resetStyle(e.target);
    })
    this.info.update();
  }

  #loadOerCountFromOersi() {
    (async () => {
      const query = {
        "size": 0,
        "aggs": {}
      }
      if (this.countrySearch) {
        query["aggs"]["countries"] = {"terms": { "field": "institutions.location.address.addressCountry", "size": 1000 }}
      }
      if (this.regionSearch) {
        query["aggs"]["states"] = {"terms": { "field": "institutions.location.address.addressRegion.keyword", "size": 1000 }}
      }
      if (this.countryRestriction) {
        query["query"] = {"term": {"institutions.location.address.addressCountry": {"value": this.countryRestriction}}}
      }
      const oersiResponse = await fetch("https://oersi.org/resources/api/search/oer_data_internal/_search",
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json"
          },
          body: JSON.stringify(query)
        }
      );
      return oersiResponse.json();
    })().then((data) => {
      this.oerCountsByRegion = Object.values(data["aggregations"]).reduce((p, v) => p.concat(v["buckets"]), []);
      const maxCount = this.oerCountsByRegion.reduce((p, v) => p > v["doc_count"] ? p : v["doc_count"], 0);
      this.#updateBoundsColors(maxCount);
      this.#updateOerCountsOnMap();
    })
  }

  #getOerCountForRegion(names, codes) {
    const regions = this.oerCountsByRegion.filter(s => Object.values(names).includes(s["key"]) || codes?.includes(s["key"]))
    if (regions) {
      return regions.reduce((accumulator, currentValue) => accumulator + currentValue["doc_count"], 0);
    }
    return 0;
  }

  #updateOerCountsOnMap() {
    this.layerData.forEach(data => {
      data["geoJsonData"].features.forEach(f => {
        const oerCount = this.#getOerCountForRegion(f.properties.names, f.properties.codes);
        f.properties = {
          ...f.properties,
          oerCount: oerCount,
          regionColor: this.#getRegionColor(oerCount)
        }
      })
      data["layer"].resetStyle();
    })
    this.legend.update();
  }

}

